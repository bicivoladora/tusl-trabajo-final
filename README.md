# Vivas, libres y federadas

## Herramientas para la comunicación de colectivas transfeministas en el Fediverso

### Resumen 

Tomando como motivación inicial un interés por construir vínculos más estrechos entre el movimiento de software libre y los activismos transfeministas, el presente trabajo propone una aproximación a las redes sociales libres y sus principales aspectos, a fin de promover su incorporación por parte de organizaciones de mujeres, colectivas disidentes y movimientos sociosexuales.

Con este propósito, se plantea un recorrido que toma como punto de partida un análisis crítico de las redes sociales comerciales, enfatizando los riesgos y desventajas que conlleva su uso para estos activismos, para luego abordar en profundidad las características distintivas de las redes sociales libres que promueven formas de comunicación más seguras, horizontales y autogestivas por parte de los colectivos. Por último, a partir de una selección de plataformas, se proponen herramientas básicas para la generación, replicación y publicación de contenido.


### Objetivos

El Trabajo Final propone como objetivo general:

- Contribuir a la incorporación de redes sociales libres por parte de organizaciones de mujeres y disidencias.

Asimismo se plantean como objetivos específicos:

- Identificar y sistematizar los principales problemas que trae aparejado el uso de las redes sociales privativas por parte de colectivas transfeministas.

- Describir el funcionamiento de las redes sociales libres de manera clara y accesible, apuntando como principal destinatario a un público no informático.

- Producir conocimiento libre que pueda ser replicado, modificado y/o adaptado por insti­tuciones, organizaciones y activistas.

Por último, se proponen como objetivos a alcanzar con posterioridad a la presentación del Trabajo:

- Diseñar e implementar instancias de formación de manera conjunta con las organizacio­nes destinatarias.

- Acompañar -de manera presencial y/o virtual- la incorporación de redes sociales libres.

- Favorecer la adopción de redes sociales libres mediante la automatización de los proce­sos de generación y publicación de contenidos.


### Metodología

El diseño y puesta en marcha del proyecto se desarrolla en tres etapas:

1. En un primer momento, se recabó información sobre el uso de redes sociales por parte de colectivas transfeministas para conocer qué plataformas utilizan habitualmente y les resultan más adecuadas para la difusión y comunicación de sus propuestas. Con estos datos, se llevó a cabo un análisis de las redes aludidas por las organizaciones, a fin de identificar aquellas características que resultan más problemáticas y perjudiciales para tales activismos. [MARZO – MAYO / 2019]

2. En segundo lugar, se realizó una indagación de las redes sociales libres, a fin de recolectar la información disponible y sistematizar su historia, principales características y funcionamiento en líneas generales, haciendo luego foco sobre aquellas plataformas libres que responden a las necesidades identificadas por las organizaciones consultadas en la primera etapa. [JUNIO – NOVIEMBRE / 2019]

3. La tercera etapa del proyecto consistió, por una parte, en elaborar una serie de materiales -en formato fanzine- para transmitir, de manera clara y accesible, los temas indagados y analizados. Por otra parte, se encuentra en curso el diseño de instancias de formación y aprendizaje colaborativo de manera conjunta con las organizaciones destinatarias. [NOVIEMBRE / 2019 y continúa]

De este modo, a la fecha de exposición, el Trabajo Final consta de:

- un documento que condensa los resultados de la primera y segunda etapas;

- una colección de fanzines dirigidos a organizaciones activistas transfeminis­tas.


### Licencia

El trabajo se encuentra disponible bajo licencia **Creative Commons Atribución-CompartirIgual 4.0 Internacional (CC BY-SA 4.0)** que permite:

*Compartir* — copiar y redistribuir el material en cualquier medio o formato.

*Adaptar* — remezclar, transformar y construir a partir del material para cualquier propósito, incluso comercialmente. 

Bajo los siguientes términos:

*Atribución* — Debe dar crédito de manera adecuada, brindar un enlace a la licencia, e indicar si se han realizado cambios. Puede hacerlo en cualquier forma razonable, pero no de forma tal que sugiera que usted o su uso tienen el apoyo de la licenciante.

*CompartirIgual* — Si remezcla, transforma o crea a partir del material, debe distribuir su contribución bajo la misma licencia del original.


### Documentación

El Trabajo Final se encuentra disponible en este repositorio y en el [repositorio de la Tecnicatura Universitaria en Software Libre](https://gitlab.com/tusl/trabajo-final).
