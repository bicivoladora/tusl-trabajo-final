## Fanzines

Los fanzines fueron diseñados para impresión monocromática en papel tamaño A3, de ambas caras, para luego doblar dos veces de modo que el tamaño final resulte en un cuarto de la hoja completa.


### Imágenes

Se referencian a continuación las imágenes utilizadas en los fanzines.

La imagen de la portada **"Vivas, libres y federadas"** es una obra con licencia CC BY-SA 4.0, derivada de los siguientes trabajos:

Marcha orgullo disidente p kindsvater 46.jpg
By Paula Kindsvater - Own work, CC BY-SA 4.0, https://commons.wikimedia.org/w/index.php?curid=74330034

8M Paraná 2019 48.jpg
By Paula Kindsvater - Own work, CC BY-SA 4.0, https://commons.wikimedia.org/w/index.php?curid=77209333

8M Paraná 2019 38.jpg
By Paula Kindsvater - Own work, CC BY-SA 4.0, https://commons.wikimedia.org/w/index.php?curid=77209312

Ni una menos parana pkindsvater 41.jpg
By Paula Kindsvater - Own work, CC BY-SA 4.0, https://commons.wikimedia.org/w/index.php?curid=69735112

Ni una menos parana pkindsvater 16.jpg
By Paula Kindsvater - Own work, CC BY-SA 4.0, https://commons.wikimedia.org/w/index.php?curid=69735087

Segundo Paro Internacional de Mujeres 8M SantaFe Argentina TitiNicola- -1-39.jpg
By TitiNicola - Own work, CC BY-SA 4.0, https://commons.wikimedia.org/w/index.php?curid=67215385

Segundo Paro Internacional de Mujeres 8M SantaFe Argentina TitiNicola- -1-82.jpg
By TitiNicola - Own work, CC BY-SA 4.0, https://commons.wikimedia.org/w/index.php?curid=67215720

Elegir Libertad - I Jornadas de Género y Software Libre - Santa Fe 20.jpg
By TitiNicola - Own work, CC BY-SA 4.0, https://commons.wikimedia.org/w/index.php?curid=67009361

Mujeres feministas en el Pañuelazo por el derecho al aborto legal, seguro y gratuito - Santa Fe - Santa Fe.jpg
By Niamfrifruli - Own work, CC BY-SA 4.0, https://commons.wikimedia.org/w/index.php?curid=68197693



En el **fanzine 1 - "Derribar la casa del amo"**, la imagen del interior es una obra con licencia CC BY-SA 4.0, derivada de los siguientes trabajos:

Audre Lorde, Meridel Lesueur, Adrienne Rich 1980.jpg
By K. Kendall - originally posted to Flickr as Audre Lorde, Meridel Lesueur, Adrienne Rich 1980, CC BY 2.0, https://commons.wikimedia.org/w/index.php?curid=8104615

Uso de dispositivos digitales - 3.jpg
By TitiNicola - Own work, CC BY-SA 4.0, https://commons.wikimedia.org/w/index.php?curid=62813307



En el **fanzine 2 - "Redes libres, redes nuestras"**, el gráfico del interior corresponde a:

Topologías de red.gif
By David de Ugarte - http://www.lasindias.net/indianopedia/Archivo:Topolog%C3%ADas_de_red.gif, Public Domain, https://commons.wikimedia.org/w/index.php?curid=2189255



En el **fanzine 3 - "Bienvenidas al Fediverso"**, los logotipos corresponden a:

Mastodon Logotype (Simple).svg
By Jin Nguyen - Mastodon’s press-kit.zip, AGPL, https://commons.wikimedia.org/w/index.php?curid=61309314

Logo de PeerTube.svg
By PeerTube contributors - logo.svg on GitHub, Public Domain, https://commons.wikimedia.org/w/index.php?curid=69871037

Pixelfed logo.svg
By Mark Wilson - https://github.com/pixelfed/pixelfed/issues/182#issuecomment-396067684, CC BY-SA 4.0, https://commons.wikimedia.org/w/index.php?curid=78190299

Le logo de Funkwhale.png
By Francis Gading - https://code.eliotberriot.com/funkwhale/funkwhale/blob/develop/front/src/assets/logo/logo.png, CC BY-SA 4.0, https://commons.wikimedia.org/w/index.php?curid=73637821

Fediverse mosaic.svg
By Tobias Buckdahn (@tobias@my.brick.camp) - https://gitlab.com/swisode/website/blob/primary/themes/quark-child/images/fediverse.svghttps://my.brick.camp/@tobias/102438886945107949, CC BY-SA 4.0, https://commons.wikimedia.org/w/index.php?curid=80417647
